
//controller for storing the data for table
function EmployeesController($scope)
{
    $scope.employees = [
        {name: 'Shreya', age: '22', gender: 'female', salary: '600', country: 'India'},
        {name: 'Alina', age: '21', gender: 'female', salary: '500', country: 'Canada'},
        {name: 'Stephen', age: '25', gender: 'male', salary: '800', country: 'USA'},
        {name: 'Heena', age: '20', gender: 'female', salary: '100', country: 'India'},
        {name: 'Geremy', age: '23', gender: 'male', salary: '200', country: 'Austria'},
        {name: 'Karan', age: '21', gender: 'male', salary: '800', country: 'India'},
        {name: 'Manan', age: '24', gender: 'male', salary: '600', country: 'India'},
        {name: 'Megha', age: '24', gender: 'male', salary: '300', country: 'India'},
        {name: 'Shane', age: '24', gender: 'male', salary: '500', country: 'Australia'},
        {name: 'Harry', age: '22', gender: 'male', salary: '700', country: 'Australia'}];
}